#!/bin/sh

if [ "$1" = "start" ]; then
    ./project/manage.py runserver 0:8001
elif  [ "$1" = "start_worker" ]; then
    ./project/manage.py get_detections
elif  [ "$1" = "setup" ]; then
    ./project/manage.py migrate --fake-initial && \
    ./project/manage.py migrate --fake && \
    ./project/manage.py runserver 0:8001
fi