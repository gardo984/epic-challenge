
import os
import json
from typing import (
    Mapping,
    Union,
    List,
    Dict,
)
from kafka import (
    KafkaConsumer,
    KafkaProducer,
)
from django.core.management.base import BaseCommand
from apps.vehicles import models as mdls


BROCKER_HOST = os.environ['BROCKER_HOST']
BROCKER_PORT = os.environ['BROCKER_PORT']
DETECTIONS_TOPIC = os.environ['DETECTIONS_TOPIC']
ALERTS_TOPIC = os.environ['ALERTS_TOPIC']
SUSPICIOUS_VEHICLE = 'SUV'

class BaseCommandMixin:

    InstanceTypes = Union[mdls.Make, mdls.Model, mdls.Category]
    InstancesCreatedTypes = Union[InstanceTypes, List[mdls.Category]]
    
    def _create_instance(
        self, 
        value: str,
        instance_type: InstanceTypes
    ) -> InstancesCreatedTypes:
        if instance_type in [mdls.Category,]:
            # validation per category, can be one or more
            values = [ x.strip() for x in value.split(",")]
            instances = []
            qs_validation = instance_type.objects.filter(
                description__in=values
            )
            if qs_validation.exists():
                return [x for x in qs_validation]
            else:
                for item in values:
                    instance = instance_type(description=item)
                    instance.save()
                    instances.append(instance)
                return instances
        else:
            # validations for model and make that usually is one
            qs_validation = instance_type.objects.filter(
                description=value
            )
            if qs_validation.exists():
                return qs_validation.first()
            else:
                instance = instance_type(description=value)
                instance.save()
                return instance

    def _create_make(self, value: str) -> mdls.Make:
        return self._create_instance(value, mdls.Make)

    def _create_model(self, value: str) -> mdls.Model:
        return self._create_instance(value, mdls.Model)

    def _create_category(self, value: str) -> List[mdls.Category]:
        return self._create_instance(value, mdls.Category)

    def create_instances(self, data: Mapping) -> Dict:
        outcome = False
        try:
            year = data.get('Year')
            make = self._create_make(data['Make'])
            model = self._create_model(data['Model'])
            categories = self._create_category(data['Category'])
            vehicle = mdls.Vehicle(
                year=year,
                make=make,
                model=model,
            )
            vehicle.save()
            for c in categories:
                vehicle.category.add(c)
            return {
                'state': True,
                'instance': vehicle,
            }
        except ValueError as ex:
            print('error: {}'.format(str(ex)))
            return {
                'state': False,
                'instance': None,
            }

    def is_evidence(self, instance: mdls.Vehicle) -> bool:
        qs_validation = (
            instance.category.filter(description=SUSPICIOUS_VEHICLE)
        )
        if qs_validation.exists():
            return True
        return False

    def get_consumer(self) -> KafkaConsumer:
        bootstrap_servers = [
            '{}:{}'.format(BROCKER_HOST, BROCKER_PORT)
        ]
        topic_name = DETECTIONS_TOPIC
        consumer = KafkaConsumer(
            topic_name,
            group_id = 'group1',
            bootstrap_servers=bootstrap_servers,
            # auto_offset_reset='earliest',
            # enable_auto_commit=False
        )
        return consumer

    def get_producer(self) -> KafkaProducer:
        bootstrap_servers = [
            '{}:{}'.format(BROCKER_HOST, BROCKER_PORT)
        ]
        producer = KafkaProducer(
            bootstrap_servers=bootstrap_servers,
            value_serializer=lambda value: json.dumps(value).encode(),
        )
        return producer

class Command(BaseCommand, BaseCommandMixin):

    help = "Get Detections from Kafka topic and generate alerts"

    def handle(self, *args, **options):
        consumer = self.get_consumer()
        for msg in consumer:
            data = json.loads(msg.value.decode())
            outcome = self.create_instances(data)
            if not outcome.get('state'):
                print('Error processing data:\n', data)
                continue

            print('Data was processed correctly:\n', data)
            if self.is_evidence(outcome.get('instance')):
                producer = self.get_producer()
                producer.send(ALERTS_TOPIC, value=data)
                print('Suspicious alert generated:\n', data)
