from django.shortcuts import render
from django.db.models import (
	Count,
	F,
	QuerySet,
	Prefetch,
)
from rest_framework.generics import ListAPIView
from rest_framework.permissions import IsAuthenticated
from apps.vehicles.serializers import (
	VehicleStatsSerializer,
	VehicleDetectionsSerializer,
)
from apps.vehicles.models import (
	Vehicle,
	Category,
)

# Create your views here.

class VehicleStatsView(ListAPIView):

    permission_classes =  [IsAuthenticated, ]
    serializer_class = VehicleStatsSerializer

    def get_queryset(self) -> QuerySet:
    	queryset = (
    		Vehicle.objects.all()
    		.select_related('make')
    		.annotate(
    			make_description=F('make__description')
    		).values(
    			'make_description'
    		).annotate(
    			vehicle_count=Count(F('id'))
    		)
    	)
    	return queryset

class VehicleDetectionsView(ListAPIView):

	permission_classes =  [IsAuthenticated, ]
	serializer_class = VehicleDetectionsSerializer

	def get_queryset(self) -> QuerySet:
		qscategory = Category.objects.all()
		queryset = (
			Vehicle.objects.all()
			.select_related('make')
			.select_related('model')
			.prefetch_related(
				Prefetch('category', queryset=qscategory)
			)
		)
		return queryset