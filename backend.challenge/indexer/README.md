
# Startup Project

## Indexer App
- Perform a build previous to start the app.
```sh
docker-compose build --no-cache --build-arg UID=$UID backend_worker
```
- Start the app services.
```sh
docker-compose up -d backend_worker
```
- Check logs:
```sh
docker-compose logs -f backend_worker
```

## Debugging

- For debugging purposes:
```sh
docker-compose run --entrypoint /bin/sh backend_worker
```