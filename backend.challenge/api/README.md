# Startup Project

## API App
- Perform a build previous to start the app.
```sh
docker-compose build --no-cache --build-arg UID=$UID backend
```
- Start the app services.
```sh
docker-compose up -d
```
> ### Optional
> If you want to create a super user:
> ```sh
> docker-compose exec backend ./project/manage.py createsuperuser
> ```

- Check logs:
```sh
docker-compose logs -f backend
```

## Debugging

- For debugging purposes:
```sh
docker-compose run --entrypoint /bin/sh backend
```