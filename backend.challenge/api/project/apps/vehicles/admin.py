from typing import List
from django.contrib import admin
from apps.vehicles import models as mdls

# Register your models here.

class AdminMixin(admin.ModelAdmin):
    def _format_created(self, obj):
        wvalue = None
        if not obj.created is None:
            wvalue = obj.created.strftime("%Y-%m-%d %H:%M:%S")
        return wvalue
    _format_created.short_description = "Created"
    _format_created.admin_order_field = "-created"

    def _format_updated(self, obj):
        wvalue = None
        if not obj.updated is None:
            wvalue = obj.updated.strftime("%Y-%m-%d %H:%M:%S")
        return wvalue
    _format_updated.short_description = "Updated"
    _format_updated.admin_order_field = "-updated"

class CategoryAdmin(AdminMixin):
    list_display = [
        "id", "description",
        "_format_created", "_format_updated",
    ]
    list_display_links = ["id", "description"]
    fields = [
        "id", "description",
        "created", "updated",
    ]
    readonly_fields = ["id", "created", "updated"]
    search_fields = ["id", "description",]
    ordering = ["-created"]
    list_per_page = 20
    empty_value_display = ""

class ModelAdmin(CategoryAdmin):
    pass

class MakeAdmin(CategoryAdmin):
    pass

class VehiclesAdmin(AdminMixin):
    list_display = [
        "id", "year", "make", "model",
        "_format_category",
        "_format_created", "_format_updated",
    ]
    list_display_links = ["id", "make"]
    fields = [
        "id", "year", "make",
        "model", "category",
        "created", "updated",
    ]
    filter_horizontal = [
        'category',
    ]
    readonly_fields = ["id", "created", "updated"]
    search_fields = [
        "id", "year", "make",
        "model", "category__description",
    ]
    ordering = ["-created"]
    list_per_page = 20
    empty_value_display = ""

    def _format_category(self, obj):
        categories: List[str] = [
            x.description
            for x in obj.category.all()
        ]
        return ', '.join(categories).strip()
    _format_category.short_description = "Category"

wmodels = [
    mdls.Category, mdls.Vehicle, mdls.Model,
    mdls.Make,
]
winterfaces = [
    CategoryAdmin, VehiclesAdmin, ModelAdmin,
    MakeAdmin,
]

for windex, witem in enumerate(wmodels):
    admin.site.register(wmodels[windex], winterfaces[windex])
