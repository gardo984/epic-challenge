
from typing import (
    Mapping,
    Dict,
    List,
)
from rest_framework import serializers
from apps.common.serializers import DynamicFieldsModelSerializer
from apps.vehicles import models as m

DATETIME_FORMAT = '%Y%m%d%H%M%s'

class MakeSerializer(DynamicFieldsModelSerializer):

    class Meta:
        model = m.Make
        fields = '__all__'

class ModelSerializer(DynamicFieldsModelSerializer):

    class Meta:
        model = m.Model
        fields = '__all__'

class CategorySerializer(DynamicFieldsModelSerializer):

    class Meta:
        model = m.Category
        fields = '__all__'
    
class VehicleStatsSerializer(serializers.Serializer):
    make_description = serializers.CharField()
    vehicle_count = serializers.IntegerField()

class VehicleDetectionsSerializer(DynamicFieldsModelSerializer):
    make = MakeSerializer(
        fields=['id', 'description', ],
        read_only=True,
    )
    model = ModelSerializer(
        fields=['id', 'description', ],
        read_only=True,
    )
    category = serializers.SerializerMethodField()

    class Meta:
        model = m.Vehicle
        fields = '__all__'

    def get_category(self, instance: m.Vehicle) -> List[Dict]:
        items = CategorySerializer(
            instance.category.all(),
            fields=['id', 'description', ],
            many=True,
        ).data
        return items