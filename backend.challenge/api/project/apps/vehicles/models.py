from datetime import datetime
from django.db import models
from apps.common.models import CommonStructure

# Create your models here.
class Make(CommonStructure):
    description = models.CharField(
        max_length=50, null=False, blank=False,
    )
    username = None
    username_modif = None

    class Meta:
        verbose_name = "Make"
        get_latest_by = ["created", ]
        ordering = ["created", ]
        indexes = [
            models.Index(fields=['created_date'],),
            models.Index(fields=['created'],),
            models.Index(fields=['updated'],),
            models.Index(fields=['description'],),
        ]

    def __str__(self):
        return '{}'.format(self.description)

    def save(self, *args, **kwargs):
        if not self.id is None:
            self.updated = datetime.now()
        super().save(*args, **kwargs)

class Model(CommonStructure):
    description = models.CharField(
        max_length=50, null=False, blank=False,
    )
    username = None
    username_modif = None

    class Meta:
        verbose_name = "Model"
        get_latest_by = ["created", ]
        ordering = ["created", ]
        indexes = [
            models.Index(fields=['created_date'],),
            models.Index(fields=['created'],),
            models.Index(fields=['updated'],),
            models.Index(fields=['description'],),
        ]

    def __str__(self):
        return '{}'.format(self.description)

    def save(self, *args, **kwargs):
        if not self.id is None:
            self.updated = datetime.now()
        super().save(*args, **kwargs)

class Category(CommonStructure):
    description = models.CharField(
        max_length=50, null=False, blank=False,
    )
    username = None
    username_modif = None

    class Meta:
        verbose_name = "Category"
        get_latest_by = ["created", ]
        ordering = ["created", ]
        indexes = [
            models.Index(fields=['created_date'],),
            models.Index(fields=['created'],),
            models.Index(fields=['updated'],),
            models.Index(fields=['description'],),
        ]

    def __str__(self):
        return '{}'.format(self.description)

    def save(self, *args, **kwargs):
        if not self.id is None:
            self.updated = datetime.now()
        super().save(*args, **kwargs)

class Vehicle(CommonStructure):
    year = models.IntegerField()
    make = models.ForeignKey(
        'vehicles.Make',
        on_delete=models.SET_NULL,
        null=True,
        blank=False,
    )
    model = models.ForeignKey(
    	'vehicles.Model',
        on_delete=models.SET_NULL,
        null=True,
        blank=False,
    )
    category = models.ManyToManyField(
        'vehicles.Category',
        related_name='vehicle_categories',
    )
    username = None
    username_modif = None

    class Meta:
        verbose_name = "Vehicles"
        get_latest_by = ["created", ]
        ordering = ["created", ]
        indexes = [
            models.Index(fields=['created_date'],),
            models.Index(fields=['created'],),
            models.Index(fields=['updated'],),
        ]

    def __str__(self):
        return '{} {}'.format(
            self.year,
            (self.make and self.make.description) or ''
        )

    def save(self, *args, **kwargs):
        if not self.id is None:
            self.updated = datetime.now()
        super().save(*args, **kwargs)
