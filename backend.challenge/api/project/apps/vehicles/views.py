import random
import time
import json
from django.shortcuts import render
from django.db.models import (
	Count,
	F,
	QuerySet,
	Prefetch,
)
from django.views import View
from django.http import (
    HttpResponse,
    StreamingHttpResponse,
)
from django.core.serializers.json import DjangoJSONEncoder
from rest_framework.generics import (
    ListAPIView,
)
from rest_framework.views import APIView
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from apps.vehicles.serializers import (
	VehicleStatsSerializer,
	VehicleDetectionsSerializer,
)
from apps.vehicles.models import (
	Vehicle,
	Category,
)
from django_eventstream import (
    send_event,
)

# Create your views here.
class AlertNotificationView(APIView):
    """
    Receive the payload registered in the database
    """
    def post(self, request):
        send_event('alerts', 'message', request.data)
        return Response(request.data)


class VehicleStatsView(ListAPIView):

    permission_classes =  [IsAuthenticated, ]
    serializer_class = VehicleStatsSerializer

    def get_queryset(self) -> QuerySet:
    	queryset = (
    		Vehicle.objects.all()
    		.select_related('make')
    		.annotate(
    			make_description=F('make__description')
    		).values(
    			'make_description'
    		).annotate(
    			vehicle_count=Count(F('id'))
    		)
    	)
    	return queryset

class VehicleDetectionsView(ListAPIView):

	permission_classes =  [IsAuthenticated, ]
	serializer_class = VehicleDetectionsSerializer

	def get_queryset(self) -> QuerySet:
		qscategory = Category.objects.all()
		queryset = (
			Vehicle.objects.all()
			.select_related('make')
			.select_related('model')
			.prefetch_related(
				Prefetch('category', queryset=qscategory)
			)
		)
		return queryset


# def event_stream_test():
#     for x in range(1,10):
#         yield f"value: {x}\n"
#         time.sleep(1)

# def event_stream():
#     initial_data = ""
#     while True:
#         new_value = random.randint(1,1000) * 0.1 
#         data = {"message": "Hello World {new_value}"}
#         print(data)
#         json_data = json.dumps(data, cls=DjangoJSONEncoder)
#         if initial_data != json_data:
#             print(json_data)
#             yield "\ndata: {json_data}\n"
#             initial_data = json_data
#         time.sleep(1)

# class AlertListingView(View):

#     def get(self, request, *args, **kwargs):
#         rsp = StreamingHttpResponse(event_stream_test())
#         # rsp = StreamingHttpResponse()
#         rsp["Content-Type"] = "text/event-stream"
#         return rsp

