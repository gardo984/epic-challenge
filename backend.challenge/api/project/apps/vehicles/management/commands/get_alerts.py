
import os
import json
import requests
from kafka import (
    KafkaConsumer,
)
from django.core.management.base import BaseCommand
from django_eventstream import (
    send_event,
)

BROCKER_HOST = os.environ['BROCKER_HOST']
BROCKER_PORT = os.environ['BROCKER_PORT']
ALERTS_TOPIC = os.environ['ALERTS_TOPIC']
endpoint = 'http://localhost:8001/api/v1/alerts'

class BaseCommandMixin:

    def get_consumer(self) -> KafkaConsumer:
        bootstrap_servers = [
            '{}:{}'.format(BROCKER_HOST, BROCKER_PORT)
        ]
        topic_name = ALERTS_TOPIC
        consumer = KafkaConsumer(
            topic_name,
            group_id = 'group1',
            bootstrap_servers=bootstrap_servers,
            # auto_offset_reset='earliest',
            # enable_auto_commit=False
        )
        return consumer


class Command(BaseCommand, BaseCommandMixin):

    help = "Get Alerts from Kafka topic"

    def handle(self, *args, **options):
        consumer = self.get_consumer()
        for msg in consumer:
            data = json.loads(msg.value.decode())
            requests.post(endpoint,data=data)
            print(data)