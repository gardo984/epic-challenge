
from django.urls import (
    include,
    path,
    re_path,
)
from apps.users.views import (
    UserCreationView,
)
from apps.vehicles.views import (
    VehicleStatsView,
    VehicleDetectionsView,
    AlertNotificationView,
)
from rest_framework_simplejwt.views import (
    TokenObtainPairView,
    TokenRefreshView,
)
from rest_framework_swagger.views import get_swagger_view

schema_view = get_swagger_view(
    title='Epic API',
)


urlpatterns = [
    re_path(r'^$', schema_view),
    path('auth/',
            include('rest_framework.urls', namespace='rest_framework')),
    path('token/', TokenObtainPairView.as_view(),
            name='token_obtain_pair'),
    path('token/refresh/', TokenRefreshView.as_view(),
            name='token_refresh'),
    path('users', UserCreationView.as_view(),
            name='user-creation'),
    path('detections', VehicleDetectionsView.as_view(), name='list-detections'),
    path('stats', VehicleStatsView.as_view(), name='display-stats'),
    path('alerts', AlertNotificationView.as_view(), name='alert-notification'),
]