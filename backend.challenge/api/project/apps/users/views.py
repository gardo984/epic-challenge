from django.shortcuts import render
from rest_framework.generics import CreateAPIView
from rest_framework.permissions import IsAuthenticated
from apps.users.serializers import UserSerializer

# Create your views here.

class UserCreationView(CreateAPIView):

    permission_classes =  [IsAuthenticated, ]
    serializer_class = UserSerializer
