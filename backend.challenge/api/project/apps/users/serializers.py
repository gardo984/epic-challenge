
import random
import base64
from functools import cached_property
from datetime import datetime
from typing import (
    Mapping,
    Dict,
)
from django.contrib.auth import models as m
from rest_framework import serializers
from rest_framework.utils.serializer_helpers import ReturnDict
from apps.common.serializers import DynamicFieldsModelSerializer

DATETIME_FORMAT = '%Y%m%d%H%M%s'

class UserSerializer(DynamicFieldsModelSerializer):
	
    username = serializers.CharField(required=False, read_only=True)

    class Meta:
        model = m.User
        fields = [
            'first_name',
            'last_name',
            'email',
            'username',
        ]

    @cached_property
    def generated_passwd(self) -> str:
        time_value = datetime.now().strftime(DATETIME_FORMAT)
        rnd_value = str(int(random.random() * 100))
        return base64.b64encode(
            '{}{}'.format(
                time_value, rnd_value
            ).encode()
        )

    @property
    def data(self):
        data = super().data
        data.update({
            'password': self.generated_passwd,
        })
        return ReturnDict(data, serializer=self)

    def build_username(self, data: Mapping) -> str:
        return '{}.{}'.format(
            data.get('first_name').lower(),
            data.get('last_name').lower()
        )

    def validate(self, data: Mapping) -> Dict:
        # validate if one of the invalid fields was specified
        invalid_fields = any([ 
            x in data  for x in ['username',]
        ])
        if invalid_fields:
            raise serializers.ValidationError(err_msg)

        # proceed with the validation of the account creation
        first_name = data.get('first_name', '')
        last_name = data.get('last_name', '')
        email = data.get('email', '')
        if not (first_name and last_name and email):
            fields = ['first_name', 'last_name', 'email',]
            err_msg = (
                'some of the required fields '
                'are empty: {}'.format(', '.join(fields))
            )
            raise serializers.ValidationError(err_msg)

        # generate the username
        data.update({
            'username': self.build_username(data),
        })
        return data

    def create(self, data: Mapping) -> m.User:
        instance = super().create(data)
        # generate the password
        instance.set_password(self.generated_passwd)
        instance.save()
        return instance
