#!/bin/sh

if [ "$1" = "start" ]; then
    ./project/manage.py get_alerts &
    ./project/manage.py runserver 0:8001

elif  [ "$1" = "migrate" ]; then
    ./project/manage.py makemigrations && \
    ./project/manage.py migrate
elif  [ "$1" = "setup" ]; then
    ./project/manage.py makemigrations && \
    ./project/manage.py migrate && \
    ./project/manage.py get_alerts &
    ./project/manage.py runserver 0:8001
fi